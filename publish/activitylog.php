<?php


return [

    'enabled' => env('ACTIVITY_LOGGER_ENABLED', true),

    // 当执行 清理命令时，删除所有指定的过期记录
    'delete_records_older_than_days' => 365,

    // 默认的日志名
    'default_log_name' => 'default',

    // 指定获取user 模型的授权驱动,不能为空
    'default_auth_driver' => null,

    // 如果为 true ,则 subject 返回软删除的模型
    'subject_returns_soft_deleted_models' => false,

    'activity_model' => \XsKit\Activitylog\Models\Activity::class,

    // 表名
    'table_name' => 'activity_log',

    /**
     * 数据库连接名
     */
    'database_connection' => env('ACTIVITY_LOGGER_DB_CONNECTION', 'default'),
];