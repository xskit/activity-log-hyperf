<?php

namespace XsKit\Activitylog;


use Hyperf\Contract\ConfigInterface;
use Hyperf\Utils\Context;

class ActivityLogStatus
{
    public function __construct(ConfigInterface $config)
    {
        $this->setEnableStatus($config->get('activitylog.enabled', true));
    }

    private function setEnableStatus($value)
    {
        Context::set(static::class . ':enable', $value);
        return $value;
    }

    private function getEnableStatus()
    {
        return Context::get(static::class . ':enable', true);
    }

    public function enable(): bool
    {
        return $this->setEnableStatus(true);
    }

    public function disable(): bool
    {
        return $this->setEnableStatus(false);
    }

    public function disabled(): bool
    {
        return $this->getEnableStatus() === false;
    }
}
