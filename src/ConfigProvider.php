<?php

namespace XsKit\Activitylog;

use Hyperf\Database\Model\Model;
use XsKit\Activitylog\Contracts\Activity;
use XsKit\Activitylog\Exceptions\InvalidConfiguration;

class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            'annotations' => [
                'scan' => [
                    'paths' => [
                        __DIR__,
                    ]
                ]
            ],
            'commands' => [
                CleanActivitylogCommand::class,
            ],
            'publish' => [
                [
                    'id' => 'config',
                    'description' => 'The config for activity log.',
                    'source' => __DIR__ . '/../publish/activitylog.php',
                    'destination' => BASE_PATH . '/config/autoload/activitylog.php',
                ],
                [
                    'id' => 'migration',
                    'description' => 'The config for activity log.',
                    'source' => __DIR__ . '/../publish/2021_04_30_175116_create_activity_log_table.php',
                    'destination' => BASE_PATH . '/migrations/2021_04_30_175116_create_activity_log_table.php',
                ],
            ],
        ];
    }

    /**
     * @return string
     * @throws InvalidConfiguration
     */
    public static function determineActivityModel(): string
    {
        $activityModel = config('activitylog.activity_model') ?? Activity::class;

        if (!is_a($activityModel, Activity::class, true)
            || !is_a($activityModel, Model::class, true)) {
            throw InvalidConfiguration::modelIsNotValid($activityModel);
        }

        return $activityModel;
    }

    /**
     * @return Activity
     * @throws InvalidConfiguration
     */
    public static function getActivityModelInstance(): Activity
    {
        $activityModelClassName = self::determineActivityModel();

        return new $activityModelClassName();
    }

}