<?php


namespace XsKit\Activitylog\Contracts;


interface Auth
{
    /**
     * 根据ID获取用户模型
     * @param $id
     * @return object|null
     */
    public function retrieveById($id);

}