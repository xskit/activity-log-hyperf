<?php

namespace XsKit\Activitylog\Traits;

use Hyperf\Database\Model\Relations\MorphMany;
use XsKit\Activitylog\ActivitylogServiceProvider;

trait CausesActivity
{
    public function actions(): MorphMany
    {
        return $this->morphMany(
            ActivitylogServiceProvider::determineActivityModel(),
            'causer'
        );
    }
}
