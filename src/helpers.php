<?php

use XsKit\Activitylog\ActivityLogger;
use XsKit\Activitylog\ActivityLogStatus;
use Hyperf\Utils\ApplicationContext;

if (!function_exists('activity')) {
    function activity(string $logName = null): ActivityLogger
    {
        $container = ApplicationContext::getContainer();
        $defaultLogName = config('activitylog.default_log_name');

        $logStatus = $container->get(ActivityLogStatus::class);

        return make(ActivityLogger::class)
            ->useLog($logName ?? $defaultLogName)
            ->setLogStatus($logStatus);
    }
}
